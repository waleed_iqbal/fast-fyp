package com.example.fyp.prototype;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;


public class ImageAdapter extends BaseAdapter {
	private Context context;
	private final String[] Values;

	public ImageAdapter(Context context, String[] mobileValues) {
		this.context = context;
		this.Values = mobileValues;
	}

	public View getView(int position, View convertView, ViewGroup parent) {

		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		View gridView;

		if (convertView == null) {

			gridView = new View(context);

			gridView = inflater.inflate(R.layout.mobile, null);

			// set value into textview
			TextView textView = (TextView) gridView
					.findViewById(R.id.grid_item_label);
			textView.setText(Values[position]);

			// set image based on selected text
			ImageView imageView = (ImageView) gridView
					.findViewById(R.id.grid_item_image);

			String mobile = Values[position];

			if (mobile.equals("Profile")) {
				imageView.setImageResource(R.drawable.profile1);
			} else if (mobile.equals("Readings")) {
				imageView.setImageResource(R.drawable.readings);
			} 

		} else {
			gridView = (View) convertView;
		}

		return gridView;
	}

	public int getCount() {
		return Values.length;
	}

	public Object getItem(int position) {
		return null;
	}

	public long getItemId(int position) {
		return 0;
	}

}
