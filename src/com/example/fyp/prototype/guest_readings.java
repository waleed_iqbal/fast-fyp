package com.example.fyp.prototype;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.example.fyp.prototype.GuestActivity.GetLogin;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.app.ActionBar.OnNavigationListener;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Toast;

public class guest_readings extends Activity {
	
	String[] actions = new String[] {
	        "Menu",
	        "Logout"
	    };
	
	int[] images = new int[]{
    		R.drawable.place
    };
			String value1[] = null;
	        String value2[] = null;
	        String value3[] = null;
	        String value4[] = null;


		@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main1);  
        int count=0;
       
        Bundle extras = getIntent().getExtras();
		  if (extras != null) {
			  
			  count=extras.getInt("count");
		     value1=new String[count];
		     value2=new String[count];
		     value3=new String[count];
		     value4=new String[count];
		      
		  }
		  value1=extras.getStringArray("Con_reading");
	      value2=extras.getStringArray("Con_month");
	      value3=extras.getStringArray("Con_image");
	      value4=extras.getStringArray("imagestatus");
		  
		  int readcount = count;
		  String fields[] = new String[readcount];
		  String info[] = new String[readcount];
		  
		  List<HashMap<String,String>> aList = new ArrayList<HashMap<String,String>>();        
		    
		  for (int i = 0; i<readcount; i++)
		  {
			  fields[i]="Reading: " + value1[i] ;
			  info[i]="Reading Month: " +  value2[i];
		  }
	
        
        for(int i=0;i<readcount;i++){
        	HashMap<String, String> hm = new HashMap<String,String>();
            hm.put("txt",  "\t"+fields[i]);
            hm.put("cur", "\t"+info[i]);
            hm.put("flag", Integer.toString(images[0]) );            
            aList.add(hm);        
        }
        
        // Keys used in Hashmap
        String[] from = { "flag","txt","cur" };
        
        int[] to = { R.id.flag,R.id.txt,R.id.cur};        
        
        SimpleAdapter adapter = new SimpleAdapter(getBaseContext(), aList, R.layout.listview_layout, from, to);
        
        ListView listView = ( ListView ) findViewById(R.id.listview);
       
        
        // Setting the adapter to the listView
        listView.setAdapter(adapter);  
        listView.setOnItemClickListener(new OnItemClickListener() {
        	   public void onItemClick(AdapterView<?> parent, View view,
        	     int position, long id) {
        	   int a=position;
        		  String imagepath= value3[a];
        		  String imagestat= value4[a];
        	   
        		  Intent i = new Intent(getApplicationContext(), Show_image.class);
        		  i.putExtra("imagepath", imagepath);
        		  i.putExtra("imagestat", imagestat);
				  	startActivity(i);
        	  }
        	  });
        
        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_spinner_dropdown_item, actions);
		 
        getActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
        
        ActionBar.OnNavigationListener navigationListener = new OnNavigationListener() {
       	 
            public boolean onNavigationItemSelected(int itemPosition, long itemId) {
                if(itemPosition==1)
                {
                	Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                	i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                	startActivity(i);
                }
                	//Toast.makeText(getBaseContext(), "You selected : " + actions[itemPosition]  , Toast.LENGTH_SHORT).show();
                return false;
            }
        };
        /** Setting dropdown items and item navigation listener for the actionbar */
     getActionBar().setListNavigationCallbacks(adapter1, navigationListener);
        
   	}
}