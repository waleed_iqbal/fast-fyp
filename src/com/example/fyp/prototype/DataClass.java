package com.example.fyp.prototype;

public class DataClass {
	  
	 static String consumer_number = null;
	 static String consumer_name = null;
	 static String consumer_address = null; 
	
	  
	 public static String getconsumer() {
	  return consumer_number;
	 }
	 public static void setconsumer(String number) {
	  consumer_number = number;
	 }
	 
	 public static String getconsumer_name() {
		  return consumer_name;
		 }
	 
	 public static void setconsumer_name(String name) {
			 consumer_name = name;
		 }
	 
	 public static String getconsumer_address() {
		  return consumer_address;
		 }
	 
	 public static void setconsumer_address(String address) {
		 consumer_address = address;
		 }
	  
	}
