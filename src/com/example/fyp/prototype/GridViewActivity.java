package com.example.fyp.prototype;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import com.example.fyp.prototype.GuestActivity.GetLogin;

import android.app.ActionBar;
import android.app.ActionBar.OnNavigationListener;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;
import android.view.ActionMode;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView.OnItemClickListener;

public class GridViewActivity extends Activity {
	String[] actions = new String[] {
	        "Menu",
	        "Logout"
	    };

	ActionMode.Callback mActionModeCallback;  
	ActionMode mActionMode;
	GridView gridView;
	private ProgressDialog pDialog;
	// JSON parser class
	JSONParser jsonParser = new JSONParser();
	EditText consumer_number_field;
//	private static final String url = "http://10.0.2.2:80/android_project/get_guest_readings.php";
	private static final String url = "http://fypproject.bugs3.com/android_project/get_guest_readings.php";
	
	private static final String TAG_SUCCESS = "success";
	private static final String returned_array = "consumer_info";
	private static final String Reading = "Reading";
	private static final String ReadingDate = "ReadingDate";
	private static final String Image = "Image";
	

	static final String[] MENU = new String[] { "Profile", "Readings" };

	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.guest_menu);

		// gets the activity's default ActionBar
		        ActionBar actionBar = getActionBar();
		        actionBar.show();

		gridView = (GridView) findViewById(R.id.gridView1);

		gridView.setAdapter(new ImageAdapter(this, MENU));

		gridView.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View v,
					int position, long id) {
				TextView abc = (TextView) v.findViewById(R.id.grid_item_label);
				String option=abc.getText().toString();
				if (option=="Profile")
				{
						String value1 = null;
				        String value2 = null;
				        String value3 = null;
				        Bundle extras = getIntent().getExtras();
						  if (extras != null) {
						      value1 = extras.getString("CN");
						      value2= extras.getString("CID");
						      value3 = extras.getString("CADD");
						  }
					Intent i = new Intent(getApplicationContext(), guestinfo1.class);
					i.putExtra("CN", value1);
					i.putExtra("CID", value2);
					i.putExtra("CADD", value3);
					
	    	      	startActivity(i);
				}
				else if (option=="Readings"){
					new GetGuest_readings().execute();
				}
					}
			
		});
		
		
///////////////////////////////////////////////////////////////////////////////////////
		
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_spinner_dropdown_item, actions);
 
        getActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
        
        ActionBar.OnNavigationListener navigationListener = new OnNavigationListener() {
       	 
            public boolean onNavigationItemSelected(int itemPosition, long itemId) {
                if(itemPosition==1)
                {
                	Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                	i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                	startActivity(i);
                }
                	//Toast.makeText(getBaseContext(), "You selected : " + actions[itemPosition]  , Toast.LENGTH_SHORT).show();
                return false;
            }
        };

 
           /** Setting dropdown items and item navigation listener for the actionbar */
        getActionBar().setListNavigationCallbacks(adapter, navigationListener);
    }
		
    

	
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	    if (keyCode == KeyEvent.KEYCODE_BACK) {
	    	moveTaskToBack(true);
	        return true;
	    }
	    return super.onKeyDown(keyCode, event);
	}
/////////////////////////////////////////////////////////////////////////////////////////////
	
	
	class GetGuest_readings extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(GridViewActivity.this);
			pDialog.setMessage("Please wait...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.show();
		}

		
		protected String doInBackground(String... args) {

					// Check for success tag
					int success;
					try {
						
						String value2 = null;
				        Bundle extras = getIntent().getExtras();
						  if (extras != null) {
						      value2= extras.getString("CID");
						    }
						
						List<NameValuePair> params = new ArrayList<NameValuePair>();
						params.add(new BasicNameValuePair("consumer_id", value2));

						JSONObject json = jsonParser.makeHttpRequest(url, "GET", params);

						// json success tag
						success = json.getInt(TAG_SUCCESS);
						if (success == 1) {
						
							DataClass con = new DataClass();
							JSONArray jsonObj = json.getJSONArray(returned_array); // JSON Array
							
							String consumer_reading[] = new String[jsonObj.length()];
							String reading_month[] = new String[jsonObj.length()];
							String month_image[] = new String[jsonObj.length()];
							String status[] = new String[jsonObj.length()];
							for(int i=0;i<jsonObj.length();i++){
							
							JSONObject con_readings = jsonObj.getJSONObject(i);
														
							consumer_reading[i]= (con_readings.getString(Reading));
							reading_month[i]= (con_readings.getString(ReadingDate));
							month_image[i]=(con_readings.getString(Image));
							status[i]=(con_readings.getString("Status"));
							}
							Intent i = new Intent(getApplicationContext(), guest_readings.class);
							i.putExtra("Con_reading", consumer_reading);
							i.putExtra("Con_month", reading_month);
						    i.putExtra("Con_image", month_image);
						    i.putExtra("imagestatus", status);
						    i.putExtra("count", jsonObj.length());
			    	      	startActivity(i);
			    	      	//finish();

						}else if(success == 0){
							// not found
							Intent i = new Intent(getApplicationContext(),
									GuestActivity.class);
							// Closing all previous activities
							i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
							startActivity(i);
							finish();	
						}
					} catch (JSONException e) {
						
						e.printStackTrace();
					}
			return null;
		}
		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String file_url) {
			pDialog.dismiss();
		}
	}
}