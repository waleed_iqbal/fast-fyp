package com.example.fyp.prototype;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
 
public class GridViewAdaptor extends BaseAdapter {
    private Context Context;
 
    // Keep all Images in array
    public Integer[] mThumbIds = {
            R.drawable.p1, R.drawable.p2,
            R.drawable.p3, R.drawable.p4,
            R.drawable.p5, R.drawable.p6
    };
 
    // Constructor
    public GridViewAdaptor(Context c){
        Context = c;
    }
 
    public int getCount() {
        return mThumbIds.length;
    }
 
    public Object getItem(int position) {
        return mThumbIds[position];
    }
 
    public long getItemId(int position) {
        return 0;
    }
 
    public View getView(int position, View convertView, ViewGroup parent) {
    	ImageView imageView;
    	  if (convertView == null) { 
              imageView = new ImageView(Context);
              imageView.setLayoutParams(new GridView.LayoutParams(150, 150));
              imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
           
          } else {
              imageView = (ImageView) convertView;
          }

    	  imageView.setImageResource(mThumbIds[position]);
          return imageView;
    }
    

	
 
}