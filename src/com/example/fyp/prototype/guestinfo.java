package com.example.fyp.prototype;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.example.fyp.prototype.GridViewActivity.GetGuest_readings;
import com.example.fyp.prototype.GuestActivity.GetLogin;
import com.example.fyp.prototype.Search.user_GetConsumer;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.app.ActionBar.OnNavigationListener;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;

public class guestinfo extends Activity {
	
	String[] actions = new String[] {
	        "Menu",
	        "Logout"
	    };
	
	private ProgressDialog pDialog;
	JSONParser jsonParser = new JSONParser();
	//private static final String url = "http://10.0.2.2:80/android_project/get_guest_readings.php";
	private static final String url = "http://fypproject.bugs3.com/android_project/get_guest_readings.php";
	
	private static final String TAG_SUCCESS = "success";
	private static final String returned_array = "consumer_info";
	private static final String Reading = "Reading";
	private static final String ReadingDate = "ReadingDate";
	private static final String Image = "Image";
	
	Boolean isInternetPresent = false;
	ConnectionDetector cd;
	
	
    String[] fields = new String[] {
            "Name:",
            "Address:",
            "Meter Number:"
    };
    
    // Array of integers points to images stored in /res/drawable-ldpi/
    int[] images = new int[]{
    		R.drawable.person,
    		R.drawable.place,
    		R.drawable.meter
    };
	
    // Array of strings to store currencies
   
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main1);     
        
        String value1 = null;
        String value2 = null;
        String value3 = null;
        Bundle extras = getIntent().getExtras();
		  if (extras != null) {
		      value1 = extras.getString("CN");
		      value2= extras.getString("CID");
		      value3 = extras.getString("CADD");
		     
		  }
	
        String[] info = new String[]{
            	value1, value3,value2};
            
        List<HashMap<String,String>> aList = new ArrayList<HashMap<String,String>>();        
        
        for(int i=0;i<3;i++){
        	HashMap<String, String> hm = new HashMap<String,String>();
            hm.put("txt",  "\t"+fields[i]);
            hm.put("cur", "\t"+info[i]);
            hm.put("flag", Integer.toString(images[i]) );            
            aList.add(hm);        
        }
        
        // Keys used in Hashmap
        String[] from = { "flag","txt","cur" };
        
        // Ids of views in listview_layout
        int[] to = { R.id.flag,R.id.txt,R.id.cur};        
        
        // Instantiating an adapter to store each items
        SimpleAdapter adapter = new SimpleAdapter(getBaseContext(), aList, R.layout.listview_layout, from, to);
        
        ListView listView = ( ListView ) findViewById(R.id.listview);
        View footerView = View.inflate(this, R.layout.footer, null);
        listView.addFooterView(footerView);
       
       Button button = (Button) findViewById(R.id.next);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Perform action on click
            	cd = new ConnectionDetector(getApplicationContext());
            	// get Internet status
				isInternetPresent = cd.isConnectingToInternet();
            	if (isInternetPresent) {
            		new GetGuest_readings().execute();
            	   	} else {
					// Internet connection is not present
					showAlertDialog(guestinfo.this, "No Internet Connection",
							"You don't have internet connection.", false);
				}
            	
            }
        }); 
        
        // Setting the adapter to the listView
        listView.setAdapter(adapter);  
        
        
		ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_spinner_dropdown_item, actions);
		 
        getActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
        
        ActionBar.OnNavigationListener navigationListener = new OnNavigationListener() {
       	 
            public boolean onNavigationItemSelected(int itemPosition, long itemId) {
                if(itemPosition==1)
                {
                	Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                	i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                	startActivity(i);
                }
                	//Toast.makeText(getBaseContext(), "You selected : " + actions[itemPosition]  , Toast.LENGTH_SHORT).show();
                return false;
            }
        };
           /** Setting dropdown items and item navigation listener for the actionbar */
        getActionBar().setListNavigationCallbacks(adapter1, navigationListener);

        
        
   	}
    
    
	//////////////////////////////////////////////////////////////////////
	@SuppressWarnings("deprecation")
	public void showAlertDialog(Context context, String title, String message, Boolean status) {
		AlertDialog alertDialog = new AlertDialog.Builder(context).create();

		// Setting Dialog Title
		alertDialog.setTitle(title);

		// Setting Dialog Message
		alertDialog.setMessage(message);
		
		// Setting alert dialog icon
		alertDialog.setIcon((status) ? R.drawable.success : R.drawable.fail);

		// Setting OK Button
		alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
			}
		});

		// Showing Alert Message
		alertDialog.show();
	}

	////////////////////////////////////////////////////////////////////////////////
    
    class GetGuest_readings extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(guestinfo.this);
			pDialog.setMessage("Please wait...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.show();
		}

		protected String doInBackground(String... args) {

					// Check for success tag
					int success;
					try {
						
						String value2 = null;
				        Bundle extras = getIntent().getExtras();
						  if (extras != null) {
						      value2= extras.getString("CID");
						    }
						
						List<NameValuePair> params = new ArrayList<NameValuePair>();
						params.add(new BasicNameValuePair("consumer_id", value2));

						JSONObject json = jsonParser.makeHttpRequest(url, "GET", params);
						
						// json success tag
						success = json.getInt(TAG_SUCCESS);
						if (success == 1) {
						
							DataClass con = new DataClass();
							JSONArray jsonObj = json.getJSONArray(returned_array); // JSON Array
							
							String consumer_reading[] = new String[jsonObj.length()];
							String reading_month[] = new String[jsonObj.length()];
							String month_image[] = new String[jsonObj.length()];
							String status[] = new String[jsonObj.length()];
							for(int i=0;i< jsonObj.length();i++){
							
							JSONObject con_readings = jsonObj.getJSONObject(i);
														
							consumer_reading[i]= (con_readings.getString(Reading));
							reading_month[i]= (con_readings.getString(ReadingDate));
							month_image[i]=(con_readings.getString(Image));
							status[i]=(con_readings.getString("Status"));
							}
							Intent i = new Intent(getApplicationContext(), guest_readings.class);
							i.putExtra("Con_reading", consumer_reading);
							i.putExtra("Con_month", reading_month);
						    i.putExtra("Con_image", month_image);
						    i.putExtra("count", jsonObj.length());
						    i.putExtra("imagestatus", status);
			    	      	startActivity(i);
			    	      //	finish();

						}else if(success == 0){
							// not found
							Intent i = new Intent(getApplicationContext(),
									GuestActivity.class);
							// Closing all previous activities
							i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
							startActivity(i);
							finish();	
						}
					} catch (JSONException e) {
						
						e.printStackTrace();
					}
				
			

			return null;
		}


		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String file_url) {
			
			pDialog.dismiss();
		}
	}

}