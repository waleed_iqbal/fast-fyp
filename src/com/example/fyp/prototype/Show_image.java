package com.example.fyp.prototype;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

import com.example.fyp.prototype.LoginActivity.GetLogin;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.ActionBar.OnNavigationListener;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class Show_image extends Activity {

	Button btnShowProgress;
	Bitmap bMap;
	Boolean isInternetPresent = false;
	ConnectionDetector cd;
	
	// Progress Dialog
	private ProgressDialog pDialog;
	ImageView my_image;
	TextView imagestatus;
	String imagestat;
	
	// Progress dialog type (0 - for Horizontal progress bar)
	public static final int progress_bar_type = 0; 
	String[] actions = new String[] {
	        "Menu",
	        "Logout"
	    };
	//private static String file_url = "http://10.0.2.2:80/";
	private static String file_url = "http://fypproject.bugs3.com/";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.show_image);
		

		my_image = (ImageView) findViewById(R.id.my_image);
		imagestatus = (TextView) findViewById(R.id.imgstatus);
		Intent i=getIntent();
		String img=i.getStringExtra("imagepath");
		imagestat=i.getStringExtra("imagestat");
		
		
		//file_url = "http://10.0.2.2:80/" + img;
		file_url = "http://fypproject.bugs3.com/" + img;
		cd = new ConnectionDetector(getApplicationContext());
    	// get Internet status
		isInternetPresent = cd.isConnectingToInternet();
    	if (isInternetPresent) {
    		new DownloadFileFromURL().execute(file_url);
    	   	} else {
			// Internet connection is not present
			showAlertDialog(Show_image.this, "No Internet Connection",
					"You don't have internet connection.", false);
		}
    	
		
    	ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_spinner_dropdown_item, actions);
		 
        getActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
        
        ActionBar.OnNavigationListener navigationListener = new OnNavigationListener() {
       	 
            public boolean onNavigationItemSelected(int itemPosition, long itemId) {
                if(itemPosition==1)
                {
                	Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                	i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                	startActivity(i);
                }
                	//Toast.makeText(getBaseContext(), "You selected : " + actions[itemPosition]  , Toast.LENGTH_SHORT).show();
                return false;
            }
        };
           /** Setting dropdown items and item navigation listener for the actionbar */
        getActionBar().setListNavigationCallbacks(adapter1, navigationListener);

	}

	/**
	 * Showing Dialog
	 * */
	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case progress_bar_type:
			pDialog = new ProgressDialog(this);
			pDialog.setMessage("Downloading file. Please wait...");
			pDialog.setIndeterminate(false);
			pDialog.setMax(100);
			pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
			pDialog.setCancelable(true);
			pDialog.show();
			return pDialog;
		default:
			return null;
		}
	}
	
	
	 /////////////////////////////////////////////////////////////////////////////////////////////////////////////
		@SuppressWarnings("deprecation")
		public void showAlertDialog(Context context, String title, String message, Boolean status) {
			AlertDialog alertDialog = new AlertDialog.Builder(context).create();

			// Setting Dialog Title
			alertDialog.setTitle(title);

			// Setting Dialog Message
			alertDialog.setMessage(message);
			
			// Setting alert dialog icon
			alertDialog.setIcon((status) ? R.drawable.success : R.drawable.fail);

			// Setting OK Button
			alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
				}
			});

			// Showing Alert Message
			alertDialog.show();
		}

	/**
	 * Background Async Task to download file
	 * */
	class DownloadFileFromURL extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread
		 * Show Progress Bar Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(Show_image.this);
			pDialog.setMessage("Please wait...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.show();
		}

		
		@Override
		protected String doInBackground(String... f_url) {
		    try {
	            URL url = new URL(f_url[0]);
	            URLConnection conection = url.openConnection();
	            conection.connect();
	  
	            // input stream to read file - with 8k buffer
	            InputStream input = new BufferedInputStream(url.openStream(), 8192);
	            BufferedInputStream buf = new BufferedInputStream(input);
	            bMap = BitmapFactory.decodeStream(buf);      
	            input.close();
	            
	        } catch (Exception e) {
	        	Log.e("Error: ", e.getMessage());
	        }
	        
	        return null;
		}
	
		
		protected void onProgressUpdate(String... progress) {
			
            pDialog.setProgress(Integer.parseInt(progress[0]));
       }

	
		@Override
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after the file was downloaded
			pDialog.dismiss();
			
			    my_image.setImageBitmap(bMap);
			    imagestatus.setText("Category: "+imagestat);
		}

	}
}