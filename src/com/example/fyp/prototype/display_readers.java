package com.example.fyp.prototype;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.example.fyp.prototype.GuestActivity.GetLogin;
import com.example.fyp.prototype.Search.user_GetReader;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.app.ActionBar.OnNavigationListener;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Toast;

public class display_readers extends Activity {
	
			boolean check=false;
			String code=null;
			// Progress Dialog
			private ProgressDialog pDialog;
			// JSON parser class
			JSONParser jsonParser = new JSONParser();
			// flag for Internet connection status
			Boolean isInternetPresent = false;
			ConnectionDetector cd;
			private static final String url1 = "http://fypproject.bugs3.com/android_project/get_reader_images.php";

			
			private static final String TAG_SUCCESS = "success";
			private static final String returned_array = "consumer_info";
			private static final String mid = "mid";
			private static final String name = "name";
			private static final String address = "address";
			private static final String image = "Image";
			

			
	String[] actions = new String[] {
	        "Menu",
	        "Logout"
	    };
	
		int[] images = new int[]{
    		R.drawable.place,
    		R.drawable.meter
    };
			String value1[] = null;
	        String value2[] = null;
	        String value3[] = null;


		@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main1);   
       
        Bundle extras = getIntent().getExtras();
		  if (extras != null) {
		      value1=extras.getStringArray("area_code");
		      value2=extras.getStringArray("full_name");
		  }
		  
		  int readcount = value1.length;
		  String fields[] = new String[readcount];
		  String info[] = new String[readcount];
		  
		  List<HashMap<String,String>> aList = new ArrayList<HashMap<String,String>>();        
		    
		  for (int i = 0; i<readcount; i++)
		  {
			  fields[i]="Area Code : " + value1[i] ;
			  info[i]="Name : " +  value2[i];
		  }
	
        
        for(int i=0;i<readcount;i++){
        	HashMap<String, String> hm = new HashMap<String,String>();
            hm.put("txt",  "\t"+fields[i]);
            hm.put("cur", "\t"+info[i]);
            hm.put("flag", Integer.toString(images[i]) );            
            aList.add(hm);        
        }
        
        String[] from = { "flag","txt","cur" };
        
        int[] to = { R.id.flag,R.id.txt,R.id.cur};        
        
        SimpleAdapter adapter = new SimpleAdapter(getBaseContext(), aList, R.layout.listview_layout, from, to);
        
        ListView listView = ( ListView ) findViewById(R.id.listview);
     
        code= value1[0];
        listView.setAdapter(adapter);  
        listView.setOnItemClickListener(new OnItemClickListener() {
        	   public void onItemClick(AdapterView<?> parent, View view,
        	     int position, long id) {
        	   int a=position;
        		  
					new user_GetReader().execute();
        	   
        	  }
        	  });
        
        
		ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_spinner_dropdown_item, actions);
		 
        getActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
        
        ActionBar.OnNavigationListener navigationListener = new OnNavigationListener() {
       	 
            public boolean onNavigationItemSelected(int itemPosition, long itemId) {
                if(itemPosition==1)
                {
                	Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                	i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                	startActivity(i);
                	
                }
                	//Toast.makeText(getBaseContext(), "You selected : " + actions[itemPosition]  , Toast.LENGTH_SHORT).show();
                return false;
            }
        };
           /** Setting dropdown items and item navigation listener for the actionbar */
        getActionBar().setListNavigationCallbacks(adapter1, navigationListener);

   	}
		
/////////////////////////////////////////////////////////////////////////////////////////////////////
		
		class user_GetReader extends AsyncTask<String, String, String> {

			/**
			 * Before starting background thread Show Progress Dialog
			 * */
			@Override
			protected void onPreExecute() {
				super.onPreExecute();
				pDialog = new ProgressDialog(display_readers.this);
				pDialog.setMessage("Please wait...");
				pDialog.setIndeterminate(false);
				pDialog.setCancelable(true);
				pDialog.show();
			}

			
			protected String doInBackground(String... args) {

						// Check for success tag
						int success;
						try {
							List<NameValuePair> params = new ArrayList<NameValuePair>();
							params.add(new BasicNameValuePair("reader_id", code));

							JSONObject json = jsonParser.makeHttpRequest(url1, "GET", params);

							
							// json success tag
							success = json.getInt(TAG_SUCCESS);
							if (success == 1) {
								// successfully login	
								

								DataClass con = new DataClass();
								JSONArray jsonObj = json.getJSONArray(returned_array); // JSON Array
								
								String images_array[] = new String[jsonObj.length()];
								String status_array[] = new String[jsonObj.length()];
								for(int i=0;i< jsonObj.length();i++){
								
								JSONObject con_readings = jsonObj.getJSONObject(i);
															
								images_array[i]= "http://fypproject.bugs3.com/"+(con_readings.getString(image));
								status_array[i]= (con_readings.getString("Status"));
								}
								Intent i = new Intent(getApplicationContext(), ImageGridActivity.class);
								i.putExtra("images", images_array);
								i.putExtra("status", status_array);
							    
				    	      	startActivity(i);
				    	
							}else if(success == 0){
								
								check=true;
								Intent i = new Intent(getApplicationContext(),Search.class);
								startActivity(i);
								
							}
						} catch (JSONException e) {
							
							e.printStackTrace();
						}
					
				

				return null;
			}


			/**
			 * After completing background task Dismiss the progress dialog
			 * **/
			protected void onPostExecute(String file_url) {
				
				pDialog.dismiss();
				if(check)
					Toast.makeText(getApplicationContext(), "Not found!", Toast.LENGTH_SHORT).show();

			}

		}	
}