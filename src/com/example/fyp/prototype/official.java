package com.example.fyp.prototype;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class official extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.official);
        
        TextView registerScreen = (TextView) findViewById(R.id.link_to_register);
        
        // Listening to login as guest link
        registerScreen.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View arg) {
				// Switching to guest screen
				Intent i = new Intent(getApplicationContext(), guestinfo.class);
				startActivity(i);
			}
		});
    }
}
