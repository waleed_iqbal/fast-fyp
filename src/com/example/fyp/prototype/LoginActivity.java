package com.example.fyp.prototype;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.example.fyp.prototype.GuestActivity.GetLogin;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class LoginActivity extends Activity {

	
	EditText email;
	EditText password;
	private ProgressDialog pDialog;
	// flag for Internet connection status
	Boolean isInternetPresent = false;
	ConnectionDetector cd;
	Boolean check = false;
	
	// JSON parser class
	JSONParser jsonParser = new JSONParser();
	//private static final String url = "http://10.0.2.2:80/android_project/get_user_login.php";
	private static final String url = "http://fypproject.bugs3.com/android_project/get_user_login.php";
	
	private static final String TAG_SUCCESS = "success";
	
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        
        TextView registerScreen = (TextView) findViewById(R.id.link_to_register);
        
        // Listening to login as guest link
        registerScreen.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View arg) {
				// Switching to guest screen
				Intent i = new Intent(getApplicationContext(), GuestActivity.class);
				startActivity(i);
			//	finish();
			}
		});
        
       email=(EditText) findViewById(R.id.Email);
       password=(EditText) findViewById(R.id.Password);
       
        Button button = (Button) findViewById(R.id.btnLogin);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	cd = new ConnectionDetector(getApplicationContext());
            	// get Internet status
				isInternetPresent = cd.isConnectingToInternet();
            	if (isInternetPresent) {
            	   	new GetLogin().execute();
            	   	} else {
					// Internet connection is not present
					showAlertDialog(LoginActivity.this, "No Internet Connection",
							"You don't have internet connection.", false);
				}

            }
        });
    }
    
    
    public boolean onKeyDown(int keyCode, KeyEvent event) {
	    if (keyCode == KeyEvent.KEYCODE_BACK) {
	    	moveTaskToBack(true);
	        return true;
	    }
	    return super.onKeyDown(keyCode, event);
	}
    
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
	@SuppressWarnings("deprecation")
	public void showAlertDialog(Context context, String title, String message, Boolean status) {
		AlertDialog alertDialog = new AlertDialog.Builder(context).create();

		// Setting Dialog Title
		alertDialog.setTitle(title);

		// Setting Dialog Message
		alertDialog.setMessage(message);
		
		// Setting alert dialog icon
		alertDialog.setIcon((status) ? R.drawable.success : R.drawable.fail);

		// Setting OK Button
		alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
			}
		});

		// Showing Alert Message
		alertDialog.show();
	}
	
    
    class GetLogin extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(LoginActivity.this);
			pDialog.setMessage("Please wait...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.show();
		}

		protected String doInBackground(String... args) {

					// Check for success tag
					int success;
					try {
						
						List<NameValuePair> params = new ArrayList<NameValuePair>();
						params.add(new BasicNameValuePair("username", email.getText().toString()));

						params.add(new BasicNameValuePair("password", password.getText().toString()));

						JSONObject json = jsonParser.makeHttpRequest(url, "POST", params);
						
						// json success tag
						success = json.getInt(TAG_SUCCESS);
						if (success == 1) {
							// successfully login	
							
							Intent i = new Intent(getApplicationContext(), Search.class);
						  	startActivity(i);
			    	      	finish();

						}else if(success == 0){
							// not found
							check = true;
							Intent i = new Intent(getApplicationContext(),
									LoginActivity.class);
							// Closing all previous activities
							i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
							startActivity(i);
							finish();	
						}
					} catch (JSONException e) {
						
						e.printStackTrace();
					}
				
			

			return null;
		}


		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String file_url) {
			
			pDialog.dismiss();
			if(check)
				Toast.makeText(getApplicationContext(), "Not found!", Toast.LENGTH_SHORT).show();

		}
	}

}
