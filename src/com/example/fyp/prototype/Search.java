package com.example.fyp.prototype;

//import com.example.intent.R;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.example.fyp.prototype.GuestActivity.GetLogin;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.app.ActionBar.OnNavigationListener;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


public class Search extends Activity implements OnClickListener{

	boolean check=false;
	boolean check1=false;
	String[] actions = new String[] {
	        "Menu",
	        "Logout"
	    };
	// Progress Dialog
		private ProgressDialog pDialog;
		// JSON parser class
		JSONParser jsonParser = new JSONParser();
		// flag for Internet connection status
		Boolean isInternetPresent = false;
		ConnectionDetector cd;
		EditText consumer_number_field;
		EditText meter_reader_field;
		//private static final String url = "http://10.0.2.2:80/android_project/get_guest_details.php";
		//private static final String url1 = "http://10.0.2.2:80/android_project/get_reader.php";
		
		private static final String url = "http://fypproject.bugs3.com/android_project/get_guest_details.php";
		private static final String url1 = "http://fypproject.bugs3.com/android_project/get_reader.php";

		
		private static final String TAG_SUCCESS = "success";
		private static final String returned_array = "consumer_info";
		private static final String mid = "mid";
		private static final String name = "name";
		private static final String address = "address";
		private static final String areacode = "AreaCode";
		private static final String fullname = "fullname";
		
		
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.search);
		Button search1result = (Button) findViewById(R.id.searchbyid);
		search1result.setOnClickListener(this);

		Button search2result = (Button) findViewById(R.id.searchbyreader);
		search2result.setOnClickListener(this);
		
		
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_spinner_dropdown_item, actions);
		 
        getActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
        
        ActionBar.OnNavigationListener navigationListener = new OnNavigationListener() {
       	 
            public boolean onNavigationItemSelected(int itemPosition, long itemId) {
                if(itemPosition==1)
                {
                	Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                	i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                	startActivity(i);
                	finish();
    				
                }
                	 return false;
            }
        };
           /** Setting dropdown items and item navigation listener for the actionbar */
        getActionBar().setListNavigationCallbacks(adapter, navigationListener);
	}
	
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	    if (keyCode == KeyEvent.KEYCODE_BACK) {
	    	moveTaskToBack(true);
	        return true;
	    }
	    return super.onKeyDown(keyCode, event);
	}

	public void onClick(View v) {
		// TODO Auto-generated method stub
		cd = new ConnectionDetector(getApplicationContext());
    	// get Internet status
		isInternetPresent = cd.isConnectingToInternet();
		
		switch (v.getId())

		{

		case R.id.searchbyid:
			final EditText cn=(EditText) findViewById(R.id.edittext1);
			if((cn.getText().toString()).equals("")){
			cn.setText("", TextView.BufferType.EDITABLE);
	        Toast.makeText(Search.this, "Please Enter Something!",Toast.LENGTH_LONG).show();

			}
			else{
				
				if (isInternetPresent) {
					new user_GetConsumer().execute();
            	   	} else {
					// Internet connection is not present
					showAlertDialog(Search.this, "No Internet Connection",
							"You don't have internet connection.", false);
				}
				
			}
			
			break;

		case R.id.searchbyreader:
			final EditText meterreader=(EditText) findViewById(R.id.edittext2);
			if((meterreader.getText().toString()).equals("")){
			meterreader.setText("", TextView.BufferType.EDITABLE);
			Toast.makeText(Search.this, "Please Enter Something!",Toast.LENGTH_LONG).show();
		}
			else{
				if (isInternetPresent) {
					new user_GetReader().execute();
            	   	} else {
					// Internet connection is not present
					showAlertDialog(Search.this, "No Internet Connection",
							"You don't have internet connection.", false);
				}
				
			}
			
			break;
		}


	}
	
	
	//////////////////////////////////////////////////////////////////////
	@SuppressWarnings("deprecation")
	public void showAlertDialog(Context context, String title, String message, Boolean status) {
		AlertDialog alertDialog = new AlertDialog.Builder(context).create();

		// Setting Dialog Title
		alertDialog.setTitle(title);

		// Setting Dialog Message
		alertDialog.setMessage(message);
		
		// Setting alert dialog icon
		alertDialog.setIcon((status) ? R.drawable.success : R.drawable.fail);

		// Setting OK Button
		alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
			}
		});

		// Showing Alert Message
		alertDialog.show();
	}

	////////////////////////////////////////////////////////////////////////////////
	class user_GetConsumer extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(Search.this);
			pDialog.setMessage("Please wait...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.show();
		}

		
		protected String doInBackground(String... args) {

					// Check for success tag
					int success;
					try {
						consumer_number_field=(EditText) findViewById(R.id.edittext1);
						List<NameValuePair> params = new ArrayList<NameValuePair>();
						params.add(new BasicNameValuePair("consumer_id", consumer_number_field.getText().toString()));
						
						JSONObject json = jsonParser.makeHttpRequest(url, "GET", params);

						
						// json success tag
						success = json.getInt(TAG_SUCCESS);
						if (success == 1) {
							// successfully login	
							
							DataClass con = new DataClass();
							JSONArray productObj = json.getJSONArray(returned_array); // JSON Array
							
							// get firstobject from JSON Array
							JSONObject product = productObj.getJSONObject(0);

							
							String consumer_name= (product.getString(name));
							String m_id= (product.getString(mid));
							String consumer_address= (product.getString(address));

							con.setconsumer(consumer_number_field.getText().toString());
							Intent i = new Intent(getApplicationContext(), guestinfo.class);
							i.putExtra("CN", consumer_name);
							i.putExtra("CID", m_id);
							i.putExtra("CADD", consumer_address);
							
			    	      	startActivity(i);
			    	  

						}else if(success == 0){
							// not found
							check1=true;
							Intent i = new Intent(getApplicationContext(),Search.class);
							
							i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
							startActivity(i);
							
						}
					} catch (JSONException e) {
						
						e.printStackTrace();
					}
				
			

			return null;
		}


		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String file_url) {
			
			pDialog.dismiss();
			if(check1)
				Toast.makeText(getApplicationContext(), "Not found!", Toast.LENGTH_SHORT).show();

		}
	}
////////////////////////////////////////////////////////////////////////////////////////////////
	
	
	class user_GetReader extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(Search.this);
			pDialog.setMessage("Please wait...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.show();
		}

		
		protected String doInBackground(String... args) {

					// Check for success tag
					int success;
					try {
						meter_reader_field=(EditText) findViewById(R.id.edittext2);
						List<NameValuePair> params = new ArrayList<NameValuePair>();
						params.add(new BasicNameValuePair("reader_id", meter_reader_field.getText().toString()));

						JSONObject json = jsonParser.makeHttpRequest(url1, "GET", params);

						
						// json success tag
						success = json.getInt(TAG_SUCCESS);
						if (success == 1) {
							// successfully login	
							

							DataClass con = new DataClass();
							JSONArray jsonObj = json.getJSONArray(returned_array); // JSON Array
							
							String area_code[] = new String[jsonObj.length()];
							String full_name[] = new String[jsonObj.length()];
							for(int i=0;i< jsonObj.length();i++){
							
							JSONObject con_readings = jsonObj.getJSONObject(i);
														
							area_code[i]= (con_readings.getString(areacode));
							full_name[i]= (con_readings.getString(fullname));
							}
							Intent i = new Intent(getApplicationContext(), display_readers.class);
							i.putExtra("area_code", area_code);
							i.putExtra("full_name", full_name);
						    
			    	      	startActivity(i);
			    	  
						}else if(success == 0){
							
							check=true;
							Intent i = new Intent(getApplicationContext(),Search.class);
						
							startActivity(i);
						}
					} catch (JSONException e) {
						
						e.printStackTrace();
					}
				
			

			return null;
		}


		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String file_url) {
			
			pDialog.dismiss();
			if(check)
				Toast.makeText(getApplicationContext(), "Not found!", Toast.LENGTH_SHORT).show();

		}
	}

}
