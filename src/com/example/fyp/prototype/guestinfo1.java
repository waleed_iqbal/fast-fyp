package com.example.fyp.prototype;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.example.fyp.prototype.GridViewActivity.GetGuest_readings;
import com.example.fyp.prototype.GuestActivity.GetLogin;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.app.ActionBar.OnNavigationListener;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;

public class guestinfo1 extends Activity {
	
	String[] actions = new String[] {
	        "Menu",
	        "Logout"
	    };
	
	private ProgressDialog pDialog;
	JSONParser jsonParser = new JSONParser();
	//private static final String url = "http://10.0.2.2:80/android_project/get_guest_readings.php";
	private static final String url = "http://fypproject.bugs3.com/android_project/get_guest_readings.php";
	private static final String TAG_SUCCESS = "success";
	private static final String returned_array = "consumer_info";
	private static final String Reading = "Reading";
	private static final String ReadingDate = "ReadingDate";
	private static final String Image = "Image";
	
	

	// Array of strings storing country names
    String[] fields = new String[] {
            "Name:",
            "Address:",
            "Meter Number:"
    };
    
    // Array of integers points to images stored in /res/drawable-ldpi/
    int[] images = new int[]{
    		R.drawable.person,
    		R.drawable.place,
    		R.drawable.meter
    };
	
    // Array of strings to store currencies
   
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main1);     
        
        String value1 = null;
        String value2 = null;
        String value3 = null;
        Bundle extras = getIntent().getExtras();
		  if (extras != null) {
		      value1 = extras.getString("CN");
		      value2= extras.getString("CID");
		      value3 = extras.getString("CADD");
		     
		  }
	
        String[] info = new String[]{
            	value1, value3,value2};
            
        List<HashMap<String,String>> aList = new ArrayList<HashMap<String,String>>();        
        
        for(int i=0;i<3;i++){
        	HashMap<String, String> hm = new HashMap<String,String>();
            hm.put("txt",  "\t"+fields[i]);
            hm.put("cur", "\t"+info[i]);
            hm.put("flag", Integer.toString(images[i]) );            
            aList.add(hm);        
        }
        String[] from = { "flag","txt","cur" };
        
        int[] to = { R.id.flag,R.id.txt,R.id.cur};        
        
        SimpleAdapter adapter = new SimpleAdapter(getBaseContext(), aList, R.layout.listview_layout, from, to);
        
        ListView listView = ( ListView ) findViewById(R.id.listview);
        
        listView.setAdapter(adapter);  
        
        
        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_spinner_dropdown_item, actions);
		 
        getActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
        
        ActionBar.OnNavigationListener navigationListener = new OnNavigationListener() {
       	 
            public boolean onNavigationItemSelected(int itemPosition, long itemId) {
                if(itemPosition==1)
                {
                	Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                	i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                	startActivity(i);
                }
                	//Toast.makeText(getBaseContext(), "You selected : " + actions[itemPosition]  , Toast.LENGTH_SHORT).show();
                return false;
            }
        };
           /** Setting dropdown items and item navigation listener for the actionbar */
        getActionBar().setListNavigationCallbacks(adapter1, navigationListener);

        
   	}

}