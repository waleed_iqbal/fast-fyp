package com.example.fyp.prototype;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.example.fyp.prototype.LoginActivity.GetLogin;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class GuestActivity extends Activity {
	
	boolean check=false;
	
	// Progress Dialog
	private ProgressDialog pDialog;
	// JSON parser class
	JSONParser jsonParser = new JSONParser();
	EditText consumer_number_field;
	//private static final String url = "http://10.0.2.2:80/android_project/get_guest_details.php";
	private static final String url = "http://fypproject.bugs3.com/android_project/get_guest_details.php";
	
	private static final String TAG_SUCCESS = "success";
	private static final String returned_array = "consumer_info";
	private static final String mid = "mid";
	private static final String name = "name";
	private static final String address = "address";
	
	Boolean isInternetPresent = false;
	ConnectionDetector cd;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Set View to register.xml
        setContentView(R.layout.register);
        
        consumer_number_field=(EditText) findViewById(R.id.consumer_number);
        
        
        TextView loginScreen = (TextView) findViewById(R.id.link_to_login);
        
        // Listening to Login Screen link
       loginScreen.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View arg0) {
				// Switching to Login Screen/closing register screen
				//finish();
				Intent i = new Intent(getApplicationContext(), LoginActivity.class);
			  	startActivity(i);
    	      	finish();
			}
	});
       
       Button btnLogin=(Button) findViewById(R.id.btnRegister);
       
       consumer_number_field=(EditText) findViewById(R.id.consumer_number);
       btnLogin.setOnClickListener(new View.OnClickListener() {
    	   
    	 
    	   public void onClick(View arg0) {
    	    // TODO Auto-generated method stub
    		   
    		   cd = new ConnectionDetector(getApplicationContext());
           	// get Internet status
				isInternetPresent = cd.isConnectingToInternet();
           	if (isInternetPresent) {
           		new GetLogin().execute();
           	   	} else {
					// Internet connection is not present
					showAlertDialog(GuestActivity.this, "No Internet Connection",
							"You don't have internet connection.", false);
				}
    }
});
       
    }
    
    
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
   	@SuppressWarnings("deprecation")
   	public void showAlertDialog(Context context, String title, String message, Boolean status) {
   		AlertDialog alertDialog = new AlertDialog.Builder(context).create();

   		// Setting Dialog Title
   		alertDialog.setTitle(title);

   		// Setting Dialog Message
   		alertDialog.setMessage(message);
   		
   		// Setting alert dialog icon
   		alertDialog.setIcon((status) ? R.drawable.success : R.drawable.fail);

   		// Setting OK Button
   		alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
   			public void onClick(DialogInterface dialog, int which) {
   			}
   		});

   		// Showing Alert Message
   		alertDialog.show();
   	}
   	

    
   	class GetLogin extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(GuestActivity.this);
			pDialog.setMessage("Please wait...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.show();
		}

		protected String doInBackground(String... args) {

					// Check for success tag
					int success;
					try {
						
						List<NameValuePair> params = new ArrayList<NameValuePair>();
						params.add(new BasicNameValuePair("consumer_id", consumer_number_field.getText().toString()));

						JSONObject json = jsonParser.makeHttpRequest(url, "GET", params);

						
						// json success tag
						success = json.getInt(TAG_SUCCESS);
						if (success == 1) {
							// successfully login	
							
							DataClass con = new DataClass();
							JSONArray productObj = json.getJSONArray(returned_array); // JSON Array
							
							// get firstobject from JSON Array
							JSONObject product = productObj.getJSONObject(0);

							
							String consumer_name= (product.getString(name));
							String m_id= (product.getString(mid));
							String consumer_address= (product.getString(address));

							con.setconsumer(consumer_number_field.getText().toString());
							Intent i = new Intent(getApplicationContext(), GridViewActivity.class);
							i.putExtra("CN", consumer_name);
							i.putExtra("CID", m_id);
							i.putExtra("CADD", consumer_address);
							
			    	      	startActivity(i);
			    	      	finish();

						}else if(success == 0){
							// not found
							check=true;
							Intent i = new Intent(getApplicationContext(),
									GuestActivity.class);
							// Closing all previous activities
							i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
							startActivity(i);
							finish();	
						}
					} catch (JSONException e) {
						
						e.printStackTrace();
					}
				
			

			return null;
		}


		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String file_url) {
			
			pDialog.dismiss();
			if(check)
				Toast.makeText(getApplicationContext(), "Not found!", Toast.LENGTH_SHORT).show();

		}
	}
}
